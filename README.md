# q3c-testutils

![License](http://img.shields.io/:license-apache-blue.svg)
[ ![Download](https://api.bintray.com/packages/dsowerby/maven/q3c-testutils/images/download.svg) ](https://bintray.com/dsowerby/maven/q3c-testutils/_latestVersion)

Some general purpose test utilities


##Gradle

```
repositories {
	jcenter()
}
```

```
'uk.q3c:q3c-testutils:x.x.x.x'
```


