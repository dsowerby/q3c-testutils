/*
 *
 *  * Copyright (c) 2016. David Sowerby
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 *  * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 *  * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 *  * specific language governing permissions and limitations under the License.
 *
 */

package uk.q3c.util.testutil

import com.google.common.collect.Lists
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.StringUtils
import java.io.File
import java.io.IOException
import java.util.*

/**
 * Provides utilities in support of testing with text files, providing line by line comparison (typically a reference and a test output)
 *
 * Modified by David Sowerby on 17/02/19
 * Created by David Sowerby on 16/11/14.
 */

object FileTestUtil {

    /**
     * Make a line by line comparison, of all lines, of the text files provided at `file1` and `file2`.  If they are not
     * the same, the comparison stops at the first mis-match and returns an Optional<String> containing a description
     * of the failure.  Lines given in `ignore` are not compared - this can be useful, for example, where a
     * line contains a timestamp. Even when a line is ignored for comparison, it must exist in both files for match
     * to succeed.
     *
     * @param ignore optional lines to ignore, index starting at 0
     * @param file1  the first file used in comparison
     * @param file2  the second file used in comparison
     * @return an empty Optional if match successful, otherwise a description of the failure
     * @throws IOException if either file is not accessible
    </String> */
    @Throws(IOException::class)
    @JvmStatic
    fun compare(file1: File, file2: File, vararg ignore: Int): Optional<String> {
        return FileTestUtil.compareFirst(-1, file1, file2, *ignore)
    }

    /**
     * Make a line by line comparison, up to and including `linesToCompare`, of the text files provided at `file1` and `file2`.
     *
     *
     * If they are not the same, the comparison stops at the first mis-match and returns an Optional<String> containing a description of the failure.  Lines
     * given in `ignore` are not compared - this can be useful, for example, where a line contains a timestamp. Even when a line is ignored for
     * comparison, it must exist in both files for match to succeed.
     *
     * @param ignore optional lines to ignore, index starting at 0
     * @param file1  the first file used in comparison
     * @param file2  the second file used in comparison
     * @return an empty Optional if match successful, otherwise a description of the failure
     * @throws IOException
    </String> */
    @Throws(IOException::class)
    @JvmStatic
    fun compareFirst(linesToCompare: Int, file1: File, file2: File, vararg ignore: Int): Optional<String> {
        val list1 = FileUtils.readLines(file1)
        val list2 = FileUtils.readLines(file2)
        return FileTestUtil.doCompareFirst(linesToCompare, list1, list2, *ignore)
    }

    private fun doCompareFirst(linesToCompare: Int, l1: List<String>, l2: List<String>, vararg ignore: Int): Optional<String> {
        var list1 = l1
        var list2 = l2
        if (linesToCompare > 0) {
            list1 = Lists.partition(list1, linesToCompare)[0]
            list2 = Lists.partition(list2, linesToCompare)[0]
        }

        val max = if (list1.size < list2.size) list1.size else list2.size
        //
        for (i in 0 until max) {
            if (!ignore.contains(i)) {
                val item1 = list1[i]
                val item2 = list2[i]

                if (item1 != item2) {
                    val comment = "line " + i + " is not the same. '" + item1 + "' ... '" + item2 + '\''.toString()
                    return Optional.of(comment)
                }
            }
        }

        if (list1.size != list2.size) {
            val comment = "Compared successfully up to line " + max + "but lists are of different size, lists 1 " +
                    "and 2 have " + list1.size + " and " + list2.size + " lines respectively"
            return Optional.of(comment)
        }
        return Optional.empty()
    }

    /**
     * Functionally equivalent to [compare] except that all blank lines are removed from
     * both files before the compare is executed.
     *
     *
     * If you use the `ignore` parameter, remember that the line number you specify needs to take account of removed
     * blank lines
     *
     * @param ignore optional lines to ignore, index starting at 0
     * @param file1  the first file used in comparison
     * @param file2  the second file used in comparison
     * @return an empty Optional if match successful, otherwise a description of the failure
     * @throws IOException if either file is not accessible
     */
    @Throws(IOException::class)
    @JvmStatic
    fun compareIgnoreBlankLines(file1: File, file2: File, vararg ignore: Int): Optional<String> {
        val list1 = FileUtils.readLines(file1)
        val list2 = FileUtils.readLines(file2)
        val list11 = FileTestUtil.removeBlankLines(list1)
        val list21 = FileTestUtil.removeBlankLines(list2)
        return FileTestUtil.doCompareFirst(-1, list11, list21, *ignore)
    }

    private fun removeBlankLines(list: List<String>): List<String> {
        val list1 = ArrayList<String>()
        for (item in list) {
            val s1 = StringUtils.strip(item)
            if (!s1.isEmpty()) {
                list1.add(item)
            }
        }
        return list1
    }

}
/**
 * Static utility only
 */
